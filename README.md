# zfs-crypt-voidlinux

## What?
Scripts to set up a Void Linux installation with an ecrypted zroot, and with zfsbootmenu and refind for booting.

Based off of multiple sources, in particular zdykstra's and emacsomancer's work, respectively: [ZFSBootMenu](https://github.com/zdykstra/zfsbootmenu) and [full ZFS and full LUKS encryption on Void Linux](https://gitlab.com/emacsomancer/full-zfs-and-full-luks-encryption-on-void-linux)