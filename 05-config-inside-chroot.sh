#!/bin/bash


# message to user
echo "This script, executed inside of the chroot, does the bulk of the basic configuration of Void Linux. Since ZFS DKMS module needs to be built against the kernel, this step will take the longest, and may represent a good time to prepare a cup of tea/coffee and a snack. There will be a large-ish amount of preparatory questions about your timezone, language, keyboard layout, set root and user password, and so on."

# timezone
echo "Please enter one of the following time regions:"
ls -l /usr/share/zoneinfo | awk '{print $9}' | tr '\n' ' '
read -p "Region: " TIMEREGION

if [[ `ls /usr/share/zoneinfo/${TIMEREGION}` ]]
then
    echo "Please enter one of the following time zones/representative cities:"
    ls -l /usr/share/zoneinfo/${TIMEREGION} | awk '{print $9}' | tr '\n' ' '
    read -p "Timezone: " TIMEZONE
else
    echo "That is not a valid region. Choosing UTC."
    TIMEREGION=UTC
fi

read -p "Please enter your desired locale/lang [en_US.UTF-8]: " LOCALECHOICE
LOCALECHOICE=${LOCALECHOICE:-"en_US.UTF-8"}

read -p "Please enter your desired keyboard layout [us] :" KEYBOARDLAY
KEYBOARDLAY=${KEYBOARDLAY:-us}

read -p "Please enter your desired system name ('host name') [myZFSVoid]: " SYSTEMNAME
SYSTEMNAME=${SYSTEMNAME:-myZFSVoid}

. importvars.sh # get old variables from previous steps too

echo "LANG=${LOCALECHOICE}" > /etc/locale.conf
echo "LC_COLLATE=C" >> /etc/locale.conf

if [ ${LOCALECHOICE} == "en_US.UTF-8" ]
then
    # echo "en_US.UTF-8 UTF-8" >> /etc/default/libc-locales # already enabled
    echo "Setting locales..."
else
    # echo "en_US.UTF-8 UTF-8" >> /etc/default/libc-locales already enabled
    echo "Setting locales..."
    echo "${LOCALECHOICE} UTF-8" >> /etc/default/libc-locales
fi
xbps-reconfigure -f glibc-locales

echo ${SYSTEMNAME} > /etc/hostname
echo "HOSTNAME=\"${SYSTEMNAME}\"" >> /etc/rc.conf
echo "HARDWARECLOCK=\"UTC\"" >> /etc/rc.conf
if [ ${TIMEREGION} == "UTC" ]
then
    echo "TIMEZONE=\"UTC\"" >> /etc/rc.conf
else
    echo "TIMEZONE=\"${TIMEREGION}/${TIMEZONE}\"" >> /etc/rc.conf
fi
echo "KEYMAP=\"${KEYBOARDLAY}\"" >> /etc/rc.conf

echo "Set new root password below "
until passwd; do echo "Confirmation didn't match. Please try again. " ; done

read -p "The following will take some time - you may want to go and put the kettle on or the like. [Press ENTER to continue.]" teatime

# install necessary packages & get updates
xbps-install -Syu xbps libxbps # update xbps
xbps-install -Syu              # update existing packages
xbps-install base-system       # install the up-to-date base system
zbps-remove base-voidstrap     # remove dated packages

# install crucial packages, including zfs, which will trigger DKMS ZFS module creation for each kernel
xbps-install -Sy linux zfs vim efibootmgr gptfdisk void-repo-nonfree
xbps-install -Sy nvidia hikari linux-firmware-intel mesa-dri vulkan-loader mesa-vulkan-intel intel-video-accel intel-ucode 

# Setting zfs cache
echo "Setting zpool cachefile for ${RPOOLNAME}."

# This seemed to mess up the boot device UUID, but I could be wrong. Anyway, it works without.
#zpool set cachefile=/etc/zfs/zpool.cache ${RPOOLNAME} # IMPORTANT

# Default boot environment
zpool set bootfs=${RPOOLNAME}/ROOT/void.${VERSION} ${RPOOLNAME}

# disable resume functionality from dracut
cat << EOF > /etc/dracut.conf.d/zol.conf
hostonly="yes"
nofsck="yes"
add_dracutmodules+=" zfs "
omit_dracutmodules+=" btrfs resume "
persistent_policy="by-id"
install_items+=" /etc/zfs/zroot.key "
EOF

TRUELINUXVERSION=`xbps-query -x linux | head -n1 | cut -f1 -d">"`
xbps-reconfigure -f ${TRUELINUXVERSION}

# inform user about resume
echo "[Resume functionality has been disabled from dracut. Otherwise future kernels which are installed will take 2-3 minutes to boot while dracut searches for the swap partition to initialise resume. This issue will be investigated, but disabling resume from dracut prevents the delayed boot.]"

read -p "Do you want to create a swap zvol? Currently, I'd recommend not doing so as swap on a zvol can be problematic. [y/N]" SWAPYN
SWAPYN=${SWAPYN:-N}

if [ ${SWAPYN} == "Y" ] || [ ${SWAPYN} == "y" ]
then
    read -p "How much swap space do you want? [8G]: " SWAPSIZE
    SWAPSIZE=${SWAPSIZE:-8G}

    # set up swap
    echo "Setting up swap and updating fstab..."
    zfs create -o sync=always -o primarycache=metadata -o secondarycache=none -b 4k -V ${SWAPSIZE} -o logbias=throughput ${RPOOLNAME}/swap
    mkswap -f /dev/zvol/${RPOOLNAME}/swap
    echo "# zvol swap vol" >> /etc/fstab
    echo "/dev/zvol/${RPOOLNAME}/swap	none	swap	sw		0	0" >> /etc/fstab
fi


# setting autotrim feature
if [[ ${ROTATIONAL} == 0 ]]
then
    read -p "You appear to be running an SSD. You can set up automatic TRIM for better wear-levelling performance. (You can always turn this feature on/off later, or handle TRIM manually.)

 Note that minimal data leakage in the form of freed block information, perhaps sufficient to determine the filesystem in use, may occur on LUKS-encrypted devices with TRIM enabled (see https://wiki.archlinux.org/index.php/Dm-crypt/Specialties#Discard/TRIM_support_for_solid_state_drives_(SSD) for more information).

Would you like to set up automatic TRIM? [Y/n]: " TRIM
    TRIM=${TRIM:-Y}
    if [ ${TRIM} == "Y" ] || [ ${TRIM} == "y" ]
    then
        zpool upgrade ${RPOOLNAME}
        sudo zpool set autotrim=on ${RPOOLNAME}
        echo "Autotrim feature enabled on ${RPOOLNAME}."
    else
        echo "Autotrim NOT enabled."
    fi
fi

# setting ARC
read -p "Would you like to set limit ARC size? (By default it will default to half available memory; if you're running a desktop/laptop, you may want to set it lower; if you're running a file-server, you may want to set it higher.) [Y/n]: "  ARCSET
ARCSET=${ARCSET:-Y}

if [ ${ARCSET} == "Y" ] || [ ${ARCSET} == "y" ]
then
    RAWMEM=`cat /proc/meminfo | grep MemTotal | awk '{print $2}'`
    #   MEMAVAIL=`echo $(((${RAWMEM}*1.025)/1024)/1024)`
    MEMAVAIL=`awk -v RAWMEM=$RAWMEM 'BEGIN {printf "%.0f\n", (((RAWMEM * 1.025)/1024) / 1024) }'`
    
    echo "You have ${MEMAVAIL}Gb available. How much memory would you like to reserve for ARC? "
    until [[ "${ARCREZ}" =~ ^[0-9]+(\.[0-9]+)?$ ]] && [[ $largerthanzero == 1 ]] && [[ $biggerthanram == 1 ]]
    do read -p "Enter a number in Gb that is larger than 0 and less than ${MEMAVAIL}, (e.g. \"4\", \"1.76\"): " ARCREZ
       largerthanzero=`awk -v ARCREZ="$ARCREZ" 'BEGIN { print (ARCREZ > 0) }'`
       biggerthanram=`awk -v ARCREZ="$ARCREZ" -v MEMAVAIL="$MEMAVAIL" 'BEGIN { print (ARCREZ<MEMAVAIL) }'`
    done
    ARCREZINT=${ARCREZ%.*}
    MEMINBYTES=`echo "$((ARCREZINT*1024*1024*1024))"`
    #    echo "
    # Limit ZFS ARC to ${ARCREZ}Gb
    # echo ${MEMINBYTES} >> /sys/module/zfs/parameters/zfs_arc_max" >> /etc/rc.local # doesn't seem to work anymore (?!) from rc.local
    # so do this instead:
    # As per: openzfs.github.io/openzfs-docs/Performance%20and%20Tuning/ZFS%20on%20Linux%20Module%20Parameters.html
    echo "options zfs zfs_arc_max=${MEMINBYTES}" >> /etc/modprobe.d/zfs.conf
    echo "${MEMINBYTES}" > /sys/modules/zfs/parameters/zfs_arc_max
fi

TRUELINUXVERSION=`xbps-query -x linux | head -n1 | cut -f1 -d">"`
xbps-reconfigure -f ${TRUELINUXVERSION}

# setting up regular user
useradd -m -s /bin/bash -U -G wheel,users,audio,video,cdrom,input ${USERNAME}
echo "Set password for user ${USERNAME}: "
until passwd ${USERNAME}; do echo "Confirmation didn't match. Please try again. " ; done

# additional configuration
read -p "Do you want to install NetworkManager? [Y/n]: " NETWORKMAN
NETWORKMAN=${NETWORKMAN:-Y}

# networkmanager configuration
if [ ${NETWORKMAN} == "Y" ] ||  [ ${NETWORKMAN} == "y" ]
then
    echo "polkit.addRule(function(action, subject) {
  if (action.id.indexOf("org.freedesktop.NetworkManager.") == 0 && subject.isInGroup("network")) {
    return polkit.Result.YES;
  }
});" > /etc/polkit-1/rules.d/50-org.freedesktop.NetworkManager.rules
    xbps-install NetworkManager
    gpasswd -a ${USERNAME} network
fi

# Install and configure ZFSBootMenu
zfs set org.zfsbootmenu:commandline="spl_hostid=$( hostid ) ro quiet" ${RPOOLNAME}/ROOT
mkfs.vfat -F32 ${ZFS_DEVICE_ID}-part1
cat << EOF >> /etc/fstab
$( blkid | grep ${ZFS_DEVICE}p1 | cut -d ' ' -f 2 ) /boot/efi vfat defaults,noauto 0 0
EOF
mkdir -p /boot/efi
mount /boot/efi

# reEFInd
xbps-install -S
xbps-install -Rs refind
refind-install
rm /boot/refind_linux.conf

# create refind conf
cat << EOF > /boot/efi/EFI/void/refind_linux.conf
"Boot default"	"zfsbootmenu:ROOT=${RBOOTNAME} spl_hostid=$( hostid ) timeout=0 ro quiet loglevel=0"
"Boot to menu"	"zfsbootmenu:ROOT=${RBOOTNAME} spl_hostid=$( hostid ) timeout=-1 ro quiet loglevel=0"
EOF

# ZFS boot menu 
xbps-install -Rs zfsbootmenu
mv /etc/zfsbootmenu/config.ini /etc/zfsbootmenu/default-config.ini
cat << EOF > /etc/zfsbootmenu/config.ini
[Global]
ManageImages=1
DracutConfDir=/etc/zfsbootmenu/dracut.conf.d
BootMountPoint=/boot/efi

[Kernel]
CommandLine=ro quiet loglevel=0

[Components]
ImageDir=/boot/efi/EFI/void
Versioned=1
Copies=3

[EFI]
ImageDir=/boot/efi/EFI/void
Versioned=1
Copies=0

[syslinux]
CreateConfig=0
Config=/boot/efi/syslinux/syslinux.cfg
EOF

# generate initial bootmenu initramfs
xbps-reconfigure -f zfsbootmenu

# export information for post-installation set-up
cat importvars.sh > /root/importvars.sh
echo "export NETWORKMAN=${NETWORKMAN}" >> /root/importvars.sh
mv 07-post-installation-setup.sh /root/07-post-installation-setup.sh

# message to user
echo "You have successfully configured Void Linux. The next step is leaving the chroot, unmounting bind mounts and exporting the ZFS pool. These actions will be performed by the next script. After exiting the chroot, please execute the following in the terminal:"
echo ". 06-umount-reboot.sh"
read -p "Press <return> to exit chroot." exitchroot

# get rid of scripts
rm importvars.sh 
mv 05-config-inside-chroot.sh /root/05-config-inside-chroot # For reference

# Create snapshot of system installation
zfs snapshot $RPOOLNAME/ROOT/void.${VERSION}@install

exit
